import typing
import unittest

import pydantic

from microwave_transistor_characterization import sweeps


class LinearSweepStepSizeTestCase(unittest.TestCase):
    def test_1(self):
        sweep_params = {
            "type_": "LinearSweepStepSize",
            "start": 10e6,
            "stop": 10e9,
            "step_size": 1e6,
        }

        sweep = sweeps.LinearSweepStepSize.PydanticModel(**sweep_params)
        sweep = sweep.instantiate()

        expected = [10e6+1e6*i for i in range(9991)]

        assert list(sweep) == expected

class LinearSweepNumStepsTestCase(unittest.TestCase):
    def test_1(self):
        sweep_params = {
            "type_": "LinearSweepNumSteps",
            "start": 10e6,
            "stop": 10e9,
            "num_steps": 9991,
        }

        sweep = sweeps.LinearSweepNumSteps.PydanticModel(**sweep_params)
        sweep = sweep.instantiate()

        expected = [10e6+1e6*i for i in range(9991)]

        assert list(sweep) == expected

class ListSweepTestCase(unittest.TestCase):
    def test_1(self):
        sweep_params = {
            "type_": "ListSweep",
            "values": [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 15.0, 28.0, 48.0],
        }

        sweep = sweeps.ListSweep.PydanticModel(**sweep_params)
        sweep = sweep.instantiate()

        expected = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 15.0, 28.0, 48.0]

        assert list(sweep) == expected


class Dummy(pydantic.BaseModel):
    thing: pydantic.Subclasses(sweeps.Sweep.PydanticModel)


class SweepSubclassTestCase(unittest.TestCase):
    def test_1(self):
        sweep_params = {
            "type_": "ListSweep",
            "values": [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 15.0, 28.0, 48.0],
        }

        Dummy.model_rebuild()
        sweep = Dummy(**{"thing": sweep_params}).thing
        sweep = sweep.instantiate()

        expected = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 15.0, 28.0, 48.0]

        assert list(sweep) == expected
