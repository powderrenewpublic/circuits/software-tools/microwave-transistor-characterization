#!/bin/bash

pytest \
    --cov-report html \
    --cov-report term \
    --cov=microwave_transistor_characterization \
    tests/microwave_transistor_characterization/sweeps.py
