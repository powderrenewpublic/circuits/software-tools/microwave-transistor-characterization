import struct

try:
    import pydantic

    from typing import Literal, ForwardRef

    from pydantic import BaseModel
    from bench_equipment.base.instrument import Instrument
    import bench_equipment.scpi
except ModuleNotFoundError:
    pydantic = False


def split_comma_track_paranthesis(some_bytes: bytes):
    if not isinstance(some_bytes, bytes):
        raise TypeError("Send me bytes!")

    results = []

    comma_value = b','[0]
    open_paranethesis_value = b'('[0]
    close_paranethesis_value = b')'[0]

    paranthesis_count = 0
    next_start = 0

    for i, byte in enumerate(some_bytes):
        if byte == comma_value:
            if paranthesis_count == 0:
                results.append(some_bytes[next_start:i])
                next_start = i+1
        elif byte == open_paranethesis_value:
            paranthesis_count += 1
        elif byte == close_paranethesis_value:
            paranthesis_count -= 1

    results.append(some_bytes[next_start:(i+1)])
    return results


class VNA:
    def __init__(self, scpi_transport):
        self.scpi_transport = scpi_transport

    ###########################################################################
    # Global Setup
    ###########################################################################
    def preset(self):
        self.scpi_transport.write(b"SYST:PRES\n")

    ###########################################################################
    # Measurement
    ###########################################################################
    def delete_all_measurements(self):
        self.scpi_transport.write(b"CALC:MEAS:DEL:ALL\n")

    def define_measurement(self,
                           measurement_number: int,
                           measurement_name: str):
        command = f'CALC:MEAS{measurement_number}:DEF "{measurement_name}"\n'
        self.scpi_transport.write(command)

    def display_measurement(self,
                            measurement_number: int,
                            window_number: int):
        command = f"DISP:MEAS{measurement_number}:FEED {window_number}\n"
        self.scpi_transport.write(command)

    def get_measurement_x_values(self, measurement_number: int):
        command = f":FORM:DATA REAL,64; :CALC:MEAS{measurement_number}:X?; "
        command += ":FORM:DATA ASC,0\n"
        self.scpi_transport.write(command)
        return self.scpi_transport.read_block_data()

    def get_measurement_sdata(self, measurement_number: int):
        command = (":FORM:DATA REAL,32; "
                   f":CALC:MEAS{measurement_number}:DATA:SDATA?; "
                   ":FORM:DATA ASC,0\n")
        self.scpi_transport.write(command)
        return self.scpi_transport.read_block_data()

    ###########################################################################
    # Averaging/Bandwidth
    ###########################################################################
    def set_low_frequency_reduced_bandwidth(self, state: bool):
        state = b'1' if state else b'0'
        command = b"SENS:BWID:TRAC "+state+b"\n"
        self.scpi_transport.write(command)

    def set_bandwidth(self, bandwidth: float):
        command = f"SENS:BWID {bandwidth}\n"
        self.scpi_transport.write(command)

    def set_averaging_count(self, count: int):
        command = f"SENS:AVER:COUN {count}\n"
        self.scpi_transport.write(command)

    def set_averaging_state(self, state: bool):
        state = b'1' if state else b'0'
        command = b"SENS:AVER "+state+b"\n"
        self.scpi_transport.write(command)

    def clear_average(self):
        self.scpi_transport.write(b"SENS:AVER:CLE")

    ###########################################################################
    # Triggering
    ###########################################################################
    def set_initiate_continuous_setting(self, initiate_continuous:bool):
        setting = b'1' if initiate_continuous else b'0'
        command = b"INIT:CONT "+setting+b"\n"
        self.scpi_transport.write(command)

    def initiate_sweep(self):
        self.scpi_transport.write(b"INIT:IMM\n")

    def initiate_sweep_and_wait(self):
        self.scpi_transport.query_ascii(b"INIT:IMM; *OPC?\n")

    ###########################################################################
    # Calibration
    ###########################################################################
    def get_calibration_error_term_names(self) -> list[bytes]:
        command = b"SENS:CORR:CSET:ETERM:CAT?\n"
        self.scpi_transport.write(command)
        catalog = self.scpi_transport.read_line()
        assert catalog[0:1] == b'"' and catalog[-1:] == b'"'
        catalog = catalog[1:-1]
        return split_comma_track_paranthesis(catalog)

    def get_calibration_error_term(self, error_term_name: bytes) -> bytes:
        command = b":FORM:DATA REAL,32; :SENS:CORR:CSET:ETERM? \""
        command += error_term_name
        command += b"\"; :FORM:DATA ASC,0\n"
        self.scpi_transport.write(command)
        return self.scpi_transport.read_block_data()

    def get_all_calibration_error_terms(self) -> dict[bytes, bytes]:
        names = self.get_calibration_error_term_names()
        return {name: self.get_calibration_error_term(name) for name in names}

    def get_calibration_stimulus(self) -> bytes:
        command = b":FORM:DATA REAL,64; :SENS:CORR:CSET:STIM?; "
        command += b":FORM:DATA ASC,0\n"
        self.scpi_transport.write(command)
        return self.scpi_transport.read_block_data()

    def create_calibration_set(self, name: str):
        command = b"SENS:CORR:CSET:CRE \"" + name.encode("ascii") + b"\"\n"
        self.scpi_transport.write(command)

    def write_error_term_data(self, error_term_name, data: bytes):
        command = (b":FORM:DATA REAL,32; :SENS:CORR:CSET:ETERM \""
                   + error_term_name.encode("ascii") + b"\",")
        command2 = b":FORM:DATA ASC,0\n"
        self.scpi_transport.write(command)
        self.scpi_transport.write_block_data(data)
        self.scpi_transport.write(command2)

    def save_calibration_set(self):
        """
        No, I don't have any clue what this means or why it's necessary...
        """
        command = b"SENS:CORR:CSET:SAVE\n"
        self.scpi_transport.write(command)

    def delete_calibration_set(self, name: str):
        command = b"CSET:DEL \"" + name.encode("ascii") + b"\"\n"
        self.scpi_transport.write(command)

    def get_calibration_set_names(self) -> list[str]:
        result = self.scpi_transport.query_ascii("CSET:CAT?\n")
        assert result[0] == '"'
        assert result[-1] == '"'
        return result[1:-1].split(",")

    ###########################################################################
    # Sweep
    ###########################################################################
    def get_sweep_type(self) -> str:
        return self.scpi_transport.query_ascii(b"SENS:SWE:TYPE?\n")

    def set_sweep_type(self, type_: str):
        command = b"SENS:SWE:TYPE " + type_.encode("ascii") + b"\n"
        self.scpi_transport.write(command)

    def get_start_frequency(self) -> float:
        # Can't use real64 because apparently sens:freq:star/stop don't use
        # form:data
        return float(self.scpi_transport.query_ascii("SENS:FREQ:STAR?\n"))

    def set_start_frequency(self, frequency: float):
        command = b"SENS:FREQ:STAR " + f"{frequency:<+014.11e}".encode("ascii")
        command += b"\n"
        self.scpi_transport.write(command)

    def get_stop_frequency(self) -> float:
        return float(self.scpi_transport.query_ascii("SENS:FREQ:STOP?\n"))

    def set_stop_frequency(self, frequency: float):
        command = b"SENS:FREQ:STOP " + f"{frequency:<+014.11e}".encode("ascii")
        command += b"\n"
        self.scpi_transport.write(command)

    def get_num_points(self) -> int:
        return int(self.scpi_transport.query_ascii("SENS:SWE:POIN?\n"))

    def set_num_points(self, points: int):
        command = b"SENS:SWE:POIN " + str(points).encode("ascii") + b"\n"
        self.scpi_transport.write(command)

    if pydantic:
        class VNA_Config(Instrument):
            driver: Literal["keysight-vna"]
            scpi: pydantic.Subclasses(bench_equipment.scpi.SCPI.SCPI_Config)

            def instantiate(self):
                return VNA(self.scpi.instantiate())
