import dataclasses
import math
import time

try:
    import pydantic

    from typing import Literal, ForwardRef

    from pydantic import BaseModel
    from bench_equipment.base.instrument import Instrument
    import bench_equipment.scpi
except ModuleNotFoundError:
    pydantic = False

from bench_equipment.base.dmm import BaseVoltageSensor, BaseCurrentSensor
from bench_equipment.base.power_supply import BaseVoltageSupply


@dataclasses.dataclass
class B296xAMeasurement:
    _mapping = {
        "VOLT": "voltage",
        "CURR": "current",
        "RES": "resistance",
        "TIME": "time",
        "STAT": "status",
        "SOUR": "source"
    }

    voltage: float = None
    current: float = None
    resistance: float = None
    time: float = None
    status: float = None
    source: float = None


# TODO: Workbench should be a context so it can clean up on exit
class B296xA:
    class Channel(BaseVoltageSupply, BaseVoltageSensor, BaseCurrentSensor):
        def __init__(self, b296Xa, channel):
            self.b296Xa = b296Xa
            self.channel = channel

        def set_voltage(self, voltage):
            commands = [f":SOUR{self.channel}:FUNC:MODE VOLT\n",
                        f":SOUR{self.channel}:VOLT {voltage}\n",
                       ]
            for command in commands:
                self.b296Xa.scpi_transport.write(command)

        def set_current_limit(self, current, side="BOTH"):
            commands = [f":SOUR{self.channel}:FUNC:MODE VOLT\n",
                        f":SENS{self.channel}:CURR:PROT:{side} {current}\n",
                       ]
            for command in commands:
                self.b296Xa.scpi_transport.write(command)

        def set_output_state(self, state):
            mode = 1 if state else 0
            commands = [f":OUTP{self.channel} {mode}\n",
                       ]
            for command in commands:
                self.b296Xa.scpi_transport.write(command)

        def get_voltage(self, **kwargs):
            return self.get_measurement("VOLT", **kwargs)

        def get_current(self, **kwargs):
            return self.get_measurement("CURR", **kwargs)

        def get_measurement(self, measurement, retries=100, retry_timeout=0.1):
            """
            Note:
                This method has the side-effect of enabling the output. I can't
                find documentation of this, but this is the behavior witnessed
                as of 12/13/2023. Might be related to :OUTP:ON/OFF:AUTO
            """
            query = f":MEAS:{measurement}? (@{self.channel})\n"

            for i in range(retries):
                result = self.b296Xa.scpi_transport.query_ascii(query)
                result = float(result)
                if abs(result) < 9e37: # Invalid results give +/- 9.9e37
                    return result
                time.sleep(retry_timeout)

            raise ValueError("Didn't get a valid result")

        def pulse_output_setup(self, base, pulse, delay, width, mode):
            """
            mode - Either "VOLT" or "CURR"
            """
            # TODO: Make generic, replace all "VOLT" with an argument
            # TODO: Write tests for e.g. when these had :SENSE instead of :SOUR
            # tests should check for errors on instrument
            commands = [f":SOUR{self.channel}:FUNC:MODE {mode}\n",
                        f":SOUR{self.channel}:FUNC:SHAP PULS\n",
                        f":SOUR{self.channel}:PULS:DEL {delay}\n",
                        f":SOUR{self.channel}:PULS:WIDT {width}\n",
                        f":SOUR{self.channel}:{mode} {base}\n",
                        f":SOUR{self.channel}:{mode}:TRIG {pulse}\n",
                       ]
            for command in commands:
                self.b296Xa.scpi_transport.write(command)

        def trigger_setup(self, delay=None, period=None, count=None,
                          trigger_output=None, qualifier="ALL"):
            """
            qualifier - one of ACQ, TRAN, ALL
            trigger_output - None for do nothing, [] for clear, or list of
                trigger outputs
            """
            trig_pre = f":TRIG{self.channel}:{qualifier}"

            commands = []

            if delay is not None:
                commands.append(f"{trig_pre}:DEL {delay}\n")
            if period is not None:
                commands.append(f"{trig_pre}:TIM {period}\n")
            if count is not None:
                commands.append(f"{trig_pre}:COUN {count}\n")
            commands.append(f"{trig_pre}:SOUR TIM\n")
            if trigger_output is not None:
                if len(trigger_output) > 0:
                    touts = ",".join(trigger_output)
                    commands.append(f"{trig_pre}:TOUT:SIGN {touts}\n")
                    commands.append(f"{trig_pre}:TOUT 1\n")
                else:
                    commands.append(f"{trig_pre}:TOUT 0\n")

            for command in commands:
                self.b296Xa.scpi_transport.write(command)

        def sense_aperture_setup(self, aperture, mode):
            """
            mode: (confusing manual seems wrong based on usage)
                    "VOLT" or "CURR" based on mode you're in (source mode)
                    Actual experience: it's a meaningless word that must be
                    either "VOLT" or "CURR" and sets both apertures together.
                    Experience was "CURR" while in voltage mode set the
                    "VOLTAGE" aperture.
            """
            commands = [f":SENS{self.channel}:{mode}:APER {aperture}\n",
                       ]
            for command in commands:
                self.b296Xa.scpi_transport.write(command)

        def sense_trigger_output(self, output_signals):
            commands = []

            if len(output_signals) > 0:
                touts = ",".join(output_signals)
                commands.append(f":SENS{self.channel}:TOUT:SIGN {touts}\n")
                commands.append(f":SENS{self.channel}:TOUT 1\n")
            else:
                commands.append(f":SENS{self.channel}:TOUT 0\n")

            for command in commands:
                self.b296Xa.scpi_transport.write(command)

        def source_trigger_output(self, output_signals):
            commands = []

            if len(output_signals) > 0:
                touts = ",".join(output_signals)
                commands.append(f":SOUR{self.channel}:TOUT:SIGN {touts}\n")
                commands.append(f":SOUR{self.channel}:TOUT 1\n")
            else:
                commands.append(f":SOUR{self.channel}:TOUT 0\n")

            for command in commands:
                self.b296Xa.scpi_transport.write(command)

        def fetch_array(self):
            query = f":FORM:ELEM:SENS?\n"
            format_ = self.b296Xa.scpi_transport.query_ascii(query)
            format_list = format_.split(",")

            query = f":FETCH:ARRAY? (@{self.channel})\n"
            results = self.b296Xa.scpi_transport.query_ascii(query)
            result_list = results.split(",")

            assert len(result_list) % len(format_list) == 0
            num_results = len(result_list) // len(format_list)

            results = []

            for i, j in zip(range(0, len(result_list), len(format_list)),
                            range(num_results)):
                obj_kwargs = {}
                for k, format_part in enumerate(format_list):
                    measurement = float(result_list[i+k])
                    if abs(measurement) > 9e37:
                        measurement = math.nan
                    obj_name = B296xAMeasurement._mapping[format_part]
                    obj_kwargs[obj_name] = measurement

                results.append(B296xAMeasurement(**obj_kwargs))

            return results

        def set_output_high_capacitance(self, high_capacitance:bool):
            commands = [f":OUTP{self.channel}:HCAP {high_capacitance}\n",
                       ]
            for command in commands:
                self.b296Xa.scpi_transport.write(command)



    if pydantic:
        class B296xA_Config(Instrument):
            driver: Literal["keysight-b296Xa"]
            scpi: pydantic.Subclasses(bench_equipment.scpi.SCPI.SCPI_Config)
            channels: int

            def instantiate(self):
                return B296xA(self.scpi.instantiate(), self.channels)

    def __init__(self, scpi_transport, channels: int):
        self.scpi_transport = scpi_transport
        self.channels = {i+1: B296xA.Channel(self, i+1)
                         for i in range(channels)}

    def blocking_operation_complete(self):
        return int(self.scpi_transport.query_ascii("*OPC?\n"))

    def dio_trigger_output_setup(self, dio_index, position="BEFORE",
                                 width=1e-5, type_="EDGE", polarity="POS"):
        assert position.upper() in ["BEFORE", "AFTER", "BOTH"]
        assert 1e-5 <= width <= 1e-2
        assert type_.upper() in ["EDGE", "LEVEL"]
        assert polarity.upper() in ["POS", "NEG"]

        commands = [f":SOUR:DIG:EXT{dio_index} TOUT\n",
                    f":SOUR:DIG:EXT{dio_index}:TOUT:POS {position}\n",
                    f":SOUR:DIG:EXT{dio_index}:TOUT:WIDTH {width}\n",
                    f":SOUR:DIG:EXT{dio_index}:TOUT:TYPE {type_}\n",
                    f":SOUR:DIG:EXT{dio_index}:POL {polarity}\n",
                       ]

        for command in commands:
            self.scpi_transport.write(command)

    def preset(self):
        self.scpi_transport.write(":SYST:PRES\n")
