import abc
from typing import TypeVar, Union

from pydantic import BaseModel

class Instrument(BaseModel, abc.ABC):
    make: str
    model: str

    @abc.abstractmethod
    def instantiate(self):
        pass
