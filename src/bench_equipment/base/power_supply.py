import abc

class BaseVoltageSupply(abc.ABC):
    @abc.abstractmethod
    def set_voltage(self, voltage):
        pass

    @abc.abstractmethod
    def set_current_limit(self, current):
        pass

    @abc.abstractmethod
    def set_output_state(self, state):
        pass
