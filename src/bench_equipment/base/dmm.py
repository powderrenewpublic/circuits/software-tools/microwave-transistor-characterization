import abc

class BaseVoltageSensor(abc.ABC):
    @abc.abstractmethod
    def get_voltage(self):
        pass

class BaseCurrentSensor(abc.ABC):
    @abc.abstractmethod
    def get_current(self):
        pass
