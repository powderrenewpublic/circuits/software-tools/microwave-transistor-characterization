import abc
import re
import socket
import time
import typing

try:
    import pydantic

    from typing import Literal, TypeVar, Union

    from pydantic import BaseModel
except ModuleNotFoundError:
    pydantic = False

_num_digits_re = re.compile(rb"\d")
_byte_count_re = re.compile(rb"\d+")

class SCPI(abc.ABC):
    read_buffer_size = 1024

    def __init__(self):
        self.buffer = b''

    @abc.abstractmethod
    def _write_bytes(self, bytes_: bytes):
        pass

    @abc.abstractmethod
    def _read_partial_bytes(self, count: int,
                            timeout: typing.Optional[float] = None) -> bytes:
        """
        This function ONLY to be called by _read_bytes of SCPI. _read_bytes
        does the buffering and keeps that consistent.

        Return at least one and up to count bytes, unless timeout is specified
        and exceeded in which case return whatever has been received (linkely
        zero bytes).
        """
        pass

    def _read_bytes(self, count: int, partial=False, timeout=None) -> bytes:
        """
        Guarantee: Count bytes will be returned unless timeout is specified and
            exceeded or partial is True, in which case however many bytes have
            been received will be returned. In the case partial and a timeout
            are specified bytes will be return as soon as anything is read and
            timeout will only be used to stop if nothing has been read.
        """
        # Hold it so we don't have to worry about other threads changing this
        read_buffer_size = self.read_buffer_size

        if timeout is not None:
            sock_timeout = self.socket.gettimeout()
            end_time = time.time() + timeout

        while len(self.buffer) < count:
            read_count = min(read_buffer_size, count)
            if partial and len(self.buffer) > 0:
                timeout_target = 0
            elif timeout is not None:
                timeout_target = end_time - time.time()
                if timeout_target <= 0:
                    break
            else:
                timeout_target = None

            tmp_buffer = self._read_partial_bytes(read_count, timeout_target)

            if timeout is not None and time.time() > end_time:
                break

            self.buffer += tmp_buffer

            if partial and len(tmp_buffer) < read_count:
                break

        result = self.buffer[:count]
        self.buffer = self.buffer[count:]
        return result

    def _put_back(self, bytes_):
        """
        Put unwanted bytes back into the buffer (at the head). Used by
        read_line for instance to put back bytes after the newline.
        """
        self.buffer = bytes_ + self.buffer

    def write(self, message):
        if isinstance(message, str):
            message = message.encode("ascii")
        self._write_bytes(message)

    def read_line(self):
        """
        Returns: The line without the terminating newline. This function
        consumes the newline from the input stream.
        """
        buffer = b''

        while b'\n' not in buffer:
            buffer += self._read_bytes(self.read_buffer_size, partial=True)

        result, buffer = buffer.split(b'\n', 1)
        self._put_back(buffer)
        return result

    def read_block_data(self) -> bytes:
        """
        Expects bytes in form of:
        #(?P<num_digits>\d)(?P<byte_count>\d{num_digits})(.{byte_count})\\n
        This function strips everything else and returns the byte_count bytes.
        """
        pound = self._read_bytes(1)
        if pound != b'#':
            raise ValueError(f"Expected '#', got '{pound}' instead!")

        num_digits = self._read_bytes(1)
        if _num_digits_re.match(num_digits) is None:
            raise ValueError(f"Expected a digit, got '{num_digits}' instead!")
        num_digits = int(num_digits)

        byte_count = self._read_bytes(num_digits)
        re_match = _byte_count_re.match(byte_count)
        if re_match is None or re_match.span() != (0, num_digits):
            raise ValueError(f"Expected {num_digits} digits, got '{byte_count}"
                             f" instead!\n{re_match}")
        byte_count = int(byte_count)

        result = self._read_bytes(byte_count)

        newline = self._read_bytes(1)
        if newline != b"\n":
            raise ValueError(f"Expected newline, got '{newline}' instead!")

        return result

    def write_block_data(self, data):
        """
        data needs to support len() and be a valid argument to socket.sendall()
        """
        bytes_count = str(len(data)).encode("ascii")
        num_digits = str(len(bytes_count)).encode("ascii")

        buffer = b'#' + num_digits + bytes_count
        self._write_bytes(buffer)
        self._write_bytes(data)
        self._write_bytes(b"\n")

    def query_ascii(self, message):
        self.write(message)
        return self.read_line().decode("ascii")

    if pydantic:
        class SCPI_Config(BaseModel):
            pass


class SCPI_Socket(SCPI):
    def __init__(self, host, port=5025):
        super().__init__()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))

    def _write_bytes(self, bytes_):
        self.socket.sendall(bytes_)

    def _read_partial_bytes(self, count: int,
                            timeout: typing.Optional[float] = None) -> bytes:
        if timeout is not None:
            sock_timeout = self.socket.gettimeout()
            self.socket.settimeout(timeout)

        try:
            result = self.socket.recv(count)
        except (TimeoutError, BlockingIOError):
            result = b''

        if timeout is not None:
            self.socket.settimeout(sock_timeout)

        return result

    if pydantic:
        class SCPI_Socket_Config(SCPI.SCPI_Config):
            type_: Literal["socket"]
            host: str
            port: int = 5025

            def instantiate(self):
                return SCPI_Socket(host=self.host, port=self.port)
