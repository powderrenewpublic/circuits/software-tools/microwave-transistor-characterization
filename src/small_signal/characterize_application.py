# We must have pydantic for this to work, so let's get at it
import pydantic

import argparse
import collections.abc
import dataclasses
import functools
import itertools
import struct
import textwrap
import typing


import pydantic.v1.utils
import yaml


from ldb.hdf5 import high_level as HDF5
from ldb.hdf5.low_level import H5L, H5O


import bench_equipment.keysight.b296Xa
import bench_equipment.keysight.vna
import bench_equipment.base.instrument
import calibration.vna
from microwave_transistor_characterization import sweeps
import terminal_menu



class SliceMeta(type):
    def __getitem__(self, key):
        return key

class Slice(metaclass=SliceMeta):
    pass


@dataclasses.dataclass
class Workbench:
    smu: bench_equipment.keysight.b296Xa.B296xA
    vna: bench_equipment.keysight.vna.VNA.VNA_Config

    class WorkbenchSetup(pydantic.BaseModel):
        smu: str
        vna: str

        def instantiate(self, workbench_inventory):
            return Workbench(smu=workbench_inventory[self.smu],
                             vna=workbench_inventory[self.vna])


class FixtureCalibrationVNASettings(pydantic.BaseModel):
    bandwidth: typing.Optional[float]
    averaging_count: typing.Optional[int]
    low_frequency_reduced_bandwidth: typing.Optional[bool]

class FixtureCalibration(pydantic.BaseModel):
    vna_settings: typing.Optional[FixtureCalibrationVNASettings]

class MeasurementParameters(pydantic.BaseModel):
    frequency_sweep: pydantic.Subclasses(sweeps.Sweep.PydanticModel)
    power: float
    fixture_calibration: typing.Optional[FixtureCalibration]


class Config(pydantic.BaseModel):
    workbench_inventory: dict[str,
                              pydantic.Subclasses(
                                  bench_equipment.base.instrument.Instrument)]
    workbench_setup: Workbench.WorkbenchSetup
    measurement_parameters: MeasurementParameters

    _instantiated_inventory = None
    _instantiated_workbench = None

    def instantiate_inventory(self):
        if self._instantiated_inventory is None:
            self._instantiated_inventory = {
                key: value.instantiate()
                for key, value in self.workbench_inventory.items()
            }

        return self._instantiated_inventory

    def instantiate_workbench(self):
        if self._instantiated_workbench is None:
            self._instantiated_workbench = \
                self.workbench_setup.instantiate(self.instantiate_inventory())

        return self._instantiated_workbench




def group_exists(hdf5_file, group):
    hdf5_file = hdf5_file.lower_object
    parts = group.split("/")
    oinfo = H5O.info2_t()

    checked_parts = []

    while len(parts) > 0:
        object_name = "/".join(checked_parts + [parts[0]])
        if not H5L.exists(hdf5_file, object_name):
            return False
        H5O.get_info_by_name3(hdf5_file, object_name, oinfo,
                              H5O.info_fields_t.BASIC)
        if oinfo.type_ != H5O.type_t.GROUP:
            return False

        checked_parts.append(parts.pop(0))

    return True

class Application:
    def __init__(self,
                 hdf5_file,
                 config: Config,
                 cable_calibration_group_name: typing.Optional[str] = None,
                 fixture_calibration_group_name: typing.Optional[str] = None):
        self.hdf5_file = hdf5_file
        self.cable_calibration_group_name = cable_calibration_group_name
        self.fixture_calibration_group_name = fixture_calibration_group_name
        self.cable_calibration_group = None
        self.fixture_calibration_group = None

        # Cable calibration has been performed and error terms have been
        # saved/loaded
        self.cable_calibrated = False
        # Fixture calibration values are in the file. Since this is post-
        # processed there is nothing to be saved/loaded.
        self.fixture_calibrated = False

        self.config = config
        self.workbench = config.instantiate_workbench()
        self.vna = self.workbench.vna

        # Setup complex datatype
        self.complex_type = HDF5.Type_.create(HDF5.H5T.CLASS.COMPOUND, 8)
        self.complex_type.ll_type.insert(
            "real", 0, HDF5.BuiltinTypes.IEEE_F32BE.ll_type)
        self.complex_type.ll_type.insert(
            "imaginary", 4, HDF5.BuiltinTypes.IEEE_F32BE.ll_type)

        self.setup_vna()

        if self.cable_calibration_group_name is not None:
            self.set_cable_calibration_group_name(
                self.cable_calibration_group_name)

        if self.fixture_calibration_group_name is not None:
            self.set_fixture_calibration_group_name(
                self.fixture_calibration_group_name)


        # TODO: Make the port numbers generic
        self.vna_measurements = {
            ("gate", "gate"): ("S11", 1),
            ("gate", "drain"): ("S12", 2),
            ("drain", "gate"): ("S21", 3),
            ("drain", "drain"): ("S22", 4),
        }

        self.vna.delete_all_measurements()

        for measurement_name, measurement_number \
                in self.vna_measurements.values():
            self.vna.define_measurement(measurement_number, measurement_name)
            self.vna.display_measurement(measurement_number, 1)

        # TODO: Set power

        default_menu_options = {
            "exit_on_shortcut": False,
            "shortcut_key_highlight_style": ("fg_green", "bold"),
            "status_bar": self.status_bar,
        }


        cable_calibration_options = {
            "[1] Set calibration group name":
                self.set_cable_calibration_group_name,
            "[2] Load from HDF5 group":
                self.menu_load_cable_calibration_from_hdf5,
            "[3] Save from VNA": self.menu_save_cable_calibration_to_hdf5,
            "[4] Copy from another HDF5 file": self.menu_copy_from_hdf5_other,
            "[x] Exit": None,
        }

        cable_calibration_menu = terminal_menu.TerminalMenuDictionary(
            cable_calibration_options,
            **default_menu_options
        )


        fixture_calibration_options = {
            "[1] Set calibration group name":
                self.set_fixture_calibration_group_name,
            "[2] Calibrate gate open":
                functools.partial(self.collect_fixture_measurement,
                                  "gate_open", "gate"),
            "[3] Calibrate gate short":
                functools.partial(self.collect_fixture_measurement,
                                  "gate_short", "gate"),
            "[4] Calibrate gate terminated in 100 ohms":
                functools.partial(self.collect_fixture_measurement,
                                  "gate_terminated_100_ohms", "gate"),
            "[5] Calibrate gate terminated in 50 ohms":
                functools.partial(self.collect_fixture_measurement,
                                  "gate_terminated_50_ohms", "gate"),
            "[6] Calibrate drain terminated in 50 ohms":
                functools.partial(self.collect_fixture_measurement,
                                  "drain_terminated_50_ohms", "drain"),
            "[7] Calibrate drain terminated in 100 ohms":
                functools.partial(self.collect_fixture_measurement,
                                  "drain_terminated_100_ohms", "drain"),
            "[8] Calibrate drain open":
                functools.partial(self.collect_fixture_measurement,
                                  "drain_open", "drain"),
            "[9] Calibrate drain short":
                functools.partial(self.collect_fixture_measurement,
                                  "drain_short", "drain"),
            "[x] Exit": None,
        }

        fixture_calibration_menu = terminal_menu.TerminalMenuDictionary(
            fixture_calibration_options,
            **default_menu_options
        )


        main_menu_options = {
            "[1] Cable Calibration": cable_calibration_menu,
            "[2] Fixture Calibration": fixture_calibration_menu,
            "[3] Run Characterization": None,
            "[x] Exit": None,
        }

        self.main_menu = terminal_menu.TerminalMenuDictionary(
            main_menu_options,
            **default_menu_options
        )

        print(type(config.measurement_parameters.frequency_sweep.instantiate()))

    def status_bar(self, menu_entry):
        cable_calibration_group = self.cable_calibration_group_name or ""
        cable_calibration_done = "X" if self.cable_calibrated else " "
        fixture_calibration_group = self.fixture_calibration_group_name or ""
        fixture_calibration_done = "X" if self.fixture_calibrated else " "
        return textwrap.dedent(f"""
            Cable Calibration   [{cable_calibration_done}] {cable_calibration_group}
            Fixture Calibration [{fixture_calibration_done}] {fixture_calibration_group}
            """.strip("\n"))

    ###########################################################################
    # Cable calibration
    ###########################################################################
    def set_cable_calibration_group_name(self, input_value: str=None):
        # TODO: Make this an interactive directory listing menu based setup
        if input_value is None:
            self.cable_calibration_group_name = input("Group name: ")
        else:
            self.cable_calibration_group_name = input_value

        self.cable_calibration_group = None
        self.cable_calibrated = False

        if group_exists(self.hdf5_file, self.cable_calibration_group_name):
            self.cable_calibration_group = self.hdf5_file.open_group(
                self.cable_calibration_group_name)
            if self.is_cable_calibration_data_complete():
                if input_value is None:
                    result = input("That group already exists, would you like "
                                   "to load calibration? ")
                else:
                    print("Loading cable calibration into VNA.")
                    result = "y"

                if result.lower() == 'y':
                    self.load_cable_calibration_from_hdf5()

    def is_cable_calibration_data_complete(self):
        if self.cable_calibration_group is None:
            return False

        required_datasets = [
            "CrossTalk(1,2)",
            "CrossTalk(2,1)",
            "Directivity(1,1)",
            "Directivity(2,2)",
            "LoadMatch(1,2)",
            "LoadMatch(2,1)",
            "ReflectionTracking(1,1)",
            "ReflectionTracking(2,2)",
            "SourceMatch(1,1)",
            "SourceMatch(2,2)",
            "Stimulus",
            "TransmissionTracking(1,2)",
            "TransmissionTracking(2,1)",
        ]

        group_contents = self.cable_calibration_group.list_group_contents()

        return set(group_contents) >= set(required_datasets)

    def check_cable_calibration_group(self):
        """
        Checks:
            * Group name set
            * Group loaded
        """
        if self.cable_calibration_group_name is None:
            print("Group name not yet specified!")
            return False

        if self.cable_calibration_group is None:
            if group_exists(self.hdf5_file, self.cable_calibration_group_name):
                self.cable_calibration_group = self.hdf5_file.open_group(
                    self.cable_calibration_group_name)
            else:
                print("Group doesn't exist!")
                return False

        return True

    def menu_load_cable_calibration_from_hdf5(self):
        if not self.check_cable_calibration_group():
            return

        if self.is_cable_calibration_data_complete():
            self.load_cable_calibration_from_hdf5()
        else:
            print("Calibration data doesn't exist or is incomplete!")
            return

    def load_cable_calibration_from_hdf5(self):
        calibration.vna.load_calibration(self.cable_calibration_group,
                                         self.vna)
        self.setup_vna_stimulus()
        self.cable_calibrated = True

    def menu_save_cable_calibration_to_hdf5(self):
        if not self.check_cable_calibration_group():
            return

        calibration.vna.save_calibration(self.cable_calibration_group,
                                         self.vna)
        self.cable_calibrated = True

    def menu_copy_from_hdf5_other(self):
        # TODO: ...
        print("Not yet implemented")

    ###########################################################################
    # Fixture calibration
    ###########################################################################
    def set_fixture_calibration_group_name(self, input_value: str=None):
        # TODO: Make this an interactive directory listing menu based setup
        if input_value is None:
            self.fixture_calibration_group_name = input("Group name: ")
        else:
            self.fixture_calibration_group_name = input_value

        self.fixture_calibration_group = None
        self.fixture_calibrated = False

        if group_exists(self.hdf5_file, self.fixture_calibration_group_name):
            self.fixture_calibration_group = self.hdf5_file.open_group(
                self.fixture_calibration_group_name)

            if self.is_fixture_calibration_data_complete():
                print("Fixture calibration found.")
                self.fixture_calibrated = True
        else:
            self.fixture_calibration_group = self.hdf5_file.create_group(
                self.fixture_calibration_group_name)

    def check_fixture_calibration_group(self):
        if self.fixture_calibration_group_name is None:
            print("Group name not yet specified!")
            return False

        if self.fixture_calibration_group is None:
            if group_exists(self.hdf5_file,
                            self.fixture_calibration_group_name):
                self.fixture_calibration_group = self.hdf5_file.open_group(
                    self.fixture_calibration_group_name)
            else:
                print("Group doesn't exist!")
                return False

        return True

    def is_fixture_calibration_data_complete(self):
        if self.fixture_calibration_group is None:
            return False

        required_datasets = [
            "gate_open",
            "gate_short",
            "gate_terminated_50_ohms",
            "gate_terminated_100_ohms",
            "drain_open",
            "drain_short",
            "drain_terminated_50_ohms",
            "drain_terminated_100_ohms",
        ]

        group_contents = self.fixture_calibration_group.list_group_contents()

        return set(group_contents) >= set(required_datasets)

    def collect_fixture_measurement(self, name, port):
        if not self.check_fixture_calibration_group():
            print("You need to specify a fixture calibration group before "
                  "calibrating!!")
            return

        hdf5_group = self.fixture_calibration_group

        fixture_calibration = self.config.measurement_parameters \
                .fixture_calibration
        if fixture_calibration is not None:
            vna_settings = fixture_calibration.vna_settings
            if vna_settings is not None:
                if vna_settings.bandwidth is not None:
                    self.vna.set_bandwidth(vna_settings.bandwidth)
                if vna_settings.averaging_count is not None:
                    self.vna.set_averaging_count(vna_settings.averaging_count)
                    self.vna.set_averaging_state(True)
                if vna_settings.low_frequency_reduced_bandwidth is not None:
                    self.vna.set_low_frequency_reduced_bandwidth(
                        vna_settings.low_frequency_reduced_bandwidth)

        with HDF5.Space.create_simple([1,1,9991]) as space, \
                HDF5.H5P.PropertyList.create(
                    HDF5.H5P.hdf5_lib.H5P_DATASET_CREATE) as ocpl:
            ocpl.set_attr_phase_change(0, 0)
            with hdf5_group.create_dataset(name, self.complex_type,
                                           space,
                                           dataset_creation_properties=ocpl
                                          ) as dataset:
                self.save_s_parameter_data_to_hdf5([port],
                                                   dataset,
                                                   ['P', 'P', 'D'])

        if self.is_fixture_calibration_data_complete():
            print("Fixture calibration complete!")
            self.fixture_calibrated = True

    ###########################################################################
    # VNA Data collection
    ###########################################################################
    def save_s_parameter_data_to_hdf5(self,
                                      ports: collections.abc.Sequence[str],
                                      dataset: HDF5.Dataset,
                                      dataset_indices:
                                        collections.abc.Sequence[int | str],
                                      averaging_count:
                                        typing.Optional[int]=None,
                                      save_attributes: bool=True):
        """
        dataset_indices:
            example - [1,2,'P','P','D'] put the port indices in the index 2,3
            axises and put the data index in axis index 4.
        """
        frequency_data = self.vna.get_measurement_x_values(1)
        num_points = len(frequency_data) // 8
        assert len(frequency_data) % 8 == 0

        if averaging_count is not None:
            self.vna.clear_average() # Belt&suspenders, should be redundant

        self.vna.set_initiate_continuous_setting(False)
        averaging_count = 1 if averaging_count is None else averaging_count
        for i in range(averaging_count):
            self.vna.initiate_sweep_and_wait()

        if save_attributes:
            with HDF5.Space.create_simple([num_points]) as attribute_space:
                with dataset.create_attribute("frequency_values",
                                              HDF5.BuiltinTypes.IEEE_F64BE,
                                              attribute_space) as attribute:
                    attribute.ll_attribute.write(
                        HDF5.BuiltinTypes.IEEE_F64BE.ll_type,
                        frequency_data)

            with HDF5.Space.create_simple([len(ports)]) as attribute_space, \
                    HDF5.BuiltinTypes.FORTRAN_S1.copy() as file_type:
                file_type.size = HDF5.H5T.VARIABLE

                with dataset.create_attribute("ports",
                                              file_type,
                                              attribute_space) as attribute:
                    data = attribute.make_data()
                    for i, port in enumerate(ports):
                        data[i] = port
                    attribute[...] = data

        for (s_out_idx, s_out), (s_in_idx, s_in) \
                in itertools.product(enumerate(ports), enumerate(ports)):
            measurement_s_parameter, measurement_number = \
                self.vna_measurements[s_out,s_in]
            indices = list(dataset_indices)
            next_port = "out"
            data_index = None
            for i, index in enumerate(indices):
                if index == "P":
                    if next_port == "out":
                        indices[i] = s_out_idx
                        next_port = "in"
                    elif next_port == "in":
                        indices[i] = s_in_idx
                        next_port = "done"
                    else:
                        raise Exception("Too many port indicies, should be 2!")
                elif index == "D":
                    indices[i] = slice(None, None, None)
                    data_index = i
            if next_port != "done" or data_index is None:
                raise Exception("You need to put 2 G's and 1 D in the dataset "
                                "indices, try harder next time!!")
            data_values = self.vna.get_measurement_sdata(measurement_number)
            file_space, mem_space, count = dataset._make_spaces(indices)
            dataset.ll_dataset.write(self.complex_type.ll_type, data_values,
                                     mem_space.ll_space, file_space.ll_space)


    ###########################################################################
    # VNA management
    ###########################################################################
    def setup_vna(self):
        self.vna.preset()
        self.setup_vna_stimulus()

    def setup_vna_stimulus(self):
        frequency_sweep = self.config.measurement_parameters.frequency_sweep \
            .instantiate()

        if isinstance(frequency_sweep, sweeps.LinearSweepNumSteps):
            self.vna.set_sweep_type("LIN")
            self.vna.set_start_frequency(frequency_sweep.start)
            self.vna.set_stop_frequency(frequency_sweep.stop)
            self.vna.set_num_points(frequency_sweep.num_steps)
        else:
            raise Exception(f"sweep type: {type(frequency_sweep)} not "
                            "supported")


def main():
    parser = argparse.ArgumentParser(
        description="""
    This is a curve tracing program. It uses an SMU to measure the ID vs
    VGS/VDS characteristics of a transistor (hence curve tracer).
    """.strip())

    # Python scoping SUCKS!
    config = [{}]

    def update_config(yaml_filename):
        with open(yaml_filename, "r") as yaml_file:
            data = yaml.load(yaml_file, Loader=yaml.Loader)

        config[0] = pydantic.v1.utils.deep_update(config[0], data)

    parser.add_argument("-f", "--yaml-file", action="append")
    parser.add_argument("--cable-calibration-group", action="store")
    parser.add_argument("--fixture-calibration-group", action="store")
    parser.add_argument("hdf5_filename")


    args = parser.parse_args()


    for yaml_filename in args.yaml_file:
        update_config(yaml_filename)

    config = config[0]


    config = Config(**config)



    with HDF5.H5P.PropertyList.create(HDF5.H5P.hdf5_lib.H5P_FILE_ACCESS) \
            as fapl:
        fapl.set_libver_bounds(HDF5.H5F.LIBVER.V18, HDF5.H5F.LIBVER.V114)
        with HDF5.File.open(args.hdf5_filename,
                            file_access_properties=fapl) as hdf5_file:
            app = Application(hdf5_file,
                              config,
                              args.cable_calibration_group,
                              args.fixture_calibration_group)
            app.main_menu.show_stack()
