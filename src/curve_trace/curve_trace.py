# We must have pydantic for this to work, so let's get at it
import pydantic

import argparse
import dataclasses
import math
import os
import statistics
import sys
import time
from typing import ForwardRef


import pydantic.v1.utils
import yaml


from ldb.hdf5 import high_level as HDF5


import bench_equipment.keysight.b296Xa
import bench_equipment.keysight.vna
import bench_equipment.base.instrument
import device_under_test.fixture.base_fet


class LinearSweep(pydantic.BaseModel):
    start: float
    stop: float
    step: float

    def __iter__(self):
        value = self.start
        while value < self.stop:
            yield value
            value += self.step

        yield self.stop # Guarantee final value is given


class SweepParameters(pydantic.BaseModel):
    v_ds_sweep: LinearSweep
    # Step up by this number of steps
    v_gs_search_steps: int
    # Until this value found
    i_d_search_threshold: float
    v_gs_step: float
    v_gs_maximum: float
    gate_high_capacitance: bool = True
    drain_high_capacitance: bool = True
    averaging_count: int
    class GatePulse(pydantic.BaseModel):
        period: float
        slope: float
        settle_time: float
        aperture: float
    gate_pulse: GatePulse


@dataclasses.dataclass
class TransistorDatapoint:
    v_gs: float
    i_d: float
    v_ds: float
    i_g: float


@dataclasses.dataclass
class Workbench:
    smu: bench_equipment.keysight.b296Xa.B296xA

    class WorkbenchSetup(pydantic.BaseModel):
        smu: str

        def instantiate(self, workbench_inventory):
            return Workbench(smu=workbench_inventory[self.smu])

    def go(self, config, hdf5_file):
        transistor_parameters = config.transistor_parameters
        sweep_parameters = config.sweep_parameters

        #######################################################################
        # Setup dataset
        #######################################################################
        V_DS_indices = len(list(sweep_parameters.v_ds_sweep))
        V_GS_indices = round((sweep_parameters.v_gs_maximum
                              - transistor_parameters.safe_v_gs) /
                             sweep_parameters.v_gs_step) + 1

        indices = [V_DS_indices,
                   V_GS_indices,
                   sweep_parameters.averaging_count]

        members = [['v_gs', HDF5.BuiltinTypes.IEEE_F64LE],
                   ['i_d', HDF5.BuiltinTypes.IEEE_F64LE],
                   ['v_ds', HDF5.BuiltinTypes.IEEE_F64LE],
                   ['i_g', HDF5.BuiltinTypes.IEEE_F64LE],
                  ]

        with HDF5.Space.create_simple(indices) as space, \
                HDF5.CompoundType.create_compound(members) as trace_type:
            # TODO: Parameterize the dataset name
            dataset = hdf5_file.create_dataset("curve_trace", trace_type,
                                               space)

            hdf5_data = dataset.make_data()

            for i,j,k in hdf5_data.indices:
                hdf5_data[i,j,k] = TransistorDatapoint(v_gs = math.nan,
                                                       i_d = math.nan,
                                                       v_ds = math.nan,
                                                       i_g = math.nan)

            dataset[...] = hdf5_data

        with HDF5.Space.create_simple([V_DS_indices]) as space:
            with hdf5_file.create_dataset("curve_trace_v_ds_target",
                    HDF5.BuiltinTypes.IEEE_F64LE,
                    space) as dataset_v_ds_target:
                data_v_ds_target = dataset_v_ds_target.make_data()
                for i, v_ds in enumerate(sweep_parameters.v_ds_sweep):
                    data_v_ds_target[i] = v_ds

        with HDF5.Space.create_simple([V_GS_indices]) as space:
            with hdf5_file.create_dataset("curve_trace_v_gs_target",
                    HDF5.BuiltinTypes.IEEE_F64LE,
                    space) as dataset_v_gs_target:
                data_v_gs_target = dataset_v_gs_target.make_data()
                for i in range(V_GS_indices):
                    v_gs = transistor_parameters.safe_v_gs + \
                                i * sweep_parameters.v_gs_step
                    data_v_gs_target[i] = v_gs


        #######################################################################
        # Setup SMU
        #######################################################################
        print(self.smu.channels[1].get_voltage())

        print("Channel 1 should be connected to the gate and channel 2 to the"
              " drain")
        while input("Are you ready? [yn] ") != 'y':
            pass

        channel_1 = self.smu.channels[1]
        channel_2 = self.smu.channels[2]

        # Safely shut everything down
        print("Setting safe gate voltage before preset")
        channel_1.set_voltage(transistor_parameters.safe_v_gs)
        time.sleep(1)
        print("Shutting off output before preset")
        channel_2.set_output_state(False)
        time.sleep(1)
        print("Preset")
        self.smu.preset()
        time.sleep(0.1)
        print("Setting safe gate voltage post preset")
        channel_1.set_voltage(transistor_parameters.safe_v_gs)


        channel_1.set_output_high_capacitance(
            sweep_parameters.gate_high_capacitance)
        channel_2.set_output_high_capacitance(
            sweep_parameters.drain_high_capacitance)

        channel_1.set_current_limit(
            transistor_parameters.absolute_maximums.i_g.minimum, "NEG")
        channel_1.set_current_limit(
            transistor_parameters.absolute_maximums.i_g.maximum, "POS")
        channel_2.set_current_limit(
            transistor_parameters.absolute_maximums.i_d.minimum, "NEG")
        channel_2.set_current_limit(
            transistor_parameters.absolute_maximums.i_d.maximum, "POS")

        # TODO: Set LFR to 60
        for channel in [channel_1, channel_2]:
            channel.trigger_setup(period=sweep_parameters.gate_pulse.period,
                                  count=sweep_parameters.averaging_count)
        #    channel.trigger_setup(delay=sweep_parameters.gate_pulse.sense_time,
        #                          qualifier="ACQ")
        #    channel.sense_aperture_setup(sweep_parameters.gate_pulse.aperture,
        #                                 "VOLT")

        #channel_1.pulse_output_setup(transistor_parameters.safe_v_gs, -2.8,
        #                             0, sweep_parameters.gate_pulse.width,
        #                             "VOLT")

        channel_1.sense_trigger_output(["EXT5"])
        self.smu.dio_trigger_output_setup(5, position="BEFORE", width=1e-5,
                                          type_="EDGE", polarity="POS")

        self.smu.scpi_transport.write(":FORM:ELEM:SENS VOLT,CURR\n")

        last = time.time()

        for v_ds_idx, v_ds in enumerate(sweep_parameters.v_ds_sweep):
            print(v_ds)
            while (v_gs_voltage := channel_1.get_voltage()) > \
                    transistor_parameters.safe_v_gs_threshold:
                print(v_gs_voltage)
                time.sleep(0.001)
            channel_2.set_voltage(v_ds)

            v_gs_idx = 0
            def v_gs_():
                return (transistor_parameters.safe_v_gs
                        + sweep_parameters.v_gs_step * v_gs_idx)
            threshold_found = False

            while v_gs_() <= sweep_parameters.v_gs_maximum:
                v_gs = v_gs_()
                hdf5_data = dataset[v_ds_idx, v_gs_idx, :]

                sense_time = sweep_parameters.gate_pulse.settle_time + \
                    (abs(v_gs - transistor_parameters.safe_v_gs) / 
                     sweep_parameters.gate_pulse.slope)
                pulse_width = sense_time + sweep_parameters.gate_pulse.aperture

                print(v_gs, sense_time, pulse_width)

                # Setup pulse voltages
                channel_1.pulse_output_setup(
                    transistor_parameters.safe_v_gs, v_gs,
                    0, pulse_width, "VOLT")
                channel_2.pulse_output_setup(
                    v_ds, v_ds, 0, pulse_width, "VOLT")

                # Setup pulse timing
                for channel in [channel_1, channel_2]:
                    channel.trigger_setup(delay=sense_time, qualifier="ACQ")
                    channel.sense_aperture_setup(
                        sweep_parameters.gate_pulse.aperture, "VOLT")

                self.smu.scpi_transport.write(":INIT (@1,2)\n")

                voltages = []
                for i, data in enumerate(channel_1.fetch_array()):
                    voltages.append(data.voltage)
                    # As of 0.0.1 LDB_HDF5 doesn't have write back through
                    # compound value, so you can't do hdf5_data[i].v_gs = ...
                    datapoint = hdf5_data[i]
                    datapoint.v_gs = data.voltage
                    datapoint.i_g = data.current
                    hdf5_data[i] = datapoint

                print(channel_2.fetch_array()[0])
                print(f"Average voltage: {statistics.mean(voltages)}")
                print(f"Voltage standard deviation: "
                      f"{statistics.stdev(voltages)}")

                currents = []
                for i, data in enumerate(channel_2.fetch_array()):
                    currents.append(data.current)
                    datapoint = hdf5_data[i]
                    datapoint.v_ds = data.voltage
                    datapoint.i_d = data.current
                    hdf5_data[i] = datapoint

                print(f"Average current: {statistics.mean(currents)}")
                print(f"Current standard deviation: "
                      f"{statistics.stdev(currents)}")

                dataset[v_ds_idx, v_gs_idx, :] = hdf5_data

                now = time.time()
                print(f"Elapsed {now-last}")
                last = now

                avg_current = statistics.mean(currents)
                if not threshold_found and \
                        avg_current >= sweep_parameters.i_d_search_threshold:
                    threshold_found = True
                    v_gs_idx -= sweep_parameters.v_gs_search_steps

                if avg_current > transistor_parameters.i_d_q_maximum:
                    break # We're done w/ this V_DS value

                if threshold_found:
                    v_gs_idx += 1
                else:
                    v_gs_idx += sweep_parameters.v_gs_search_steps


class Config(pydantic.BaseModel):
    workbench_inventory: dict[str,
                              pydantic.Subclasses(
                                  bench_equipment.base.instrument.Instrument)]
    workbench_setup: Workbench.WorkbenchSetup
    transistor_parameters: device_under_test.fixture.base_fet.BaseFETParameters
    sweep_parameters: SweepParameters

    model_config = pydantic.ConfigDict(defer_build=True)

    _instantiated_inventory = None
    _instantiated_workbench = None

    def instantiate_inventory(self):
        if self._instantiated_inventory is None:
            self._instantiated_inventory = {
                key: value.instantiate()
                for key, value in self.workbench_inventory.items()
            }

        return self._instantiated_inventory

    def instantiate_workbench(self):
        if self._instantiated_workbench is None:
            self._instantiated_workbench = \
                self.workbench_setup.instantiate(self.instantiate_inventory())

        return self._instantiated_workbench




def main():
    parser = argparse.ArgumentParser(
        description="""
    This is a curve tracing program. It uses an SMU to measure the ID vs
    VGS/VDS characteristics of a transistor (hence curve tracer).
    """.strip())

    # Python scoping SUCKS!
    config = [{}]

    def update_config(yaml_filename):
        with open(yaml_filename, "r") as yaml_file:
            data = yaml.load(yaml_file, Loader=yaml.Loader)

        config[0] = pydantic.v1.utils.deep_update(config[0], data)

    parser.add_argument("-f", "--yaml-file", action="append")
    parser.add_argument("hdf5_filename")

    args = parser.parse_args()

    for yaml_filename in args.yaml_file:
        update_config(yaml_filename)

    config = config[0]

    config = Config(**config)

    workbench = config.instantiate_workbench()

    if os.path.exists(args.hdf5_filename):
        print(f"File {args.hdf5_filename} already exists.")
        response = input("Would you like to delete it? [yN] ")
        if response.lower() == 'y':
            os.remove(args.hdf5_filename)
        else:
            print("Can't proceed, delete or try a new filename")
            sys.exit(1)

    with HDF5.File.open(args.hdf5_filename) as hdf5_file:
        workbench.go(config, hdf5_file)
