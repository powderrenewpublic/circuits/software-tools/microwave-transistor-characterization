import abc

try:
    import pydantic

    from typing import Literal, ForwardRef, TypeVar, Union

    from pydantic import BaseModel
except ModuleNotFoundError:
    pydantic = False


class Sweep(abc.ABC):
    @abc.abstractmethod
    def __iter__(self): # pragma: no cover
        yield None

    if pydantic:
        class PydanticModel(pydantic.BaseModel, abc.ABC):
            # type_ is the discriminator

            @abc.abstractmethod
            def instantiate(self):
                pass


class LinearSweepStepSize(Sweep):
    start: float
    stop: float
    step_size: float

    def __init__(self, start: float, stop: float, step_size: float):
        self.start = start
        self.stop = stop
        self.step_size = step_size

    def __iter__(self):
        value = self.start
        while value < self.stop:
            yield value
            value += self.step_size

        yield self.stop # Guarantee final value is given

    if pydantic:
        class PydanticModel(Sweep.PydanticModel):
            type_: Literal["LinearSweepStepSize"]
            start: float
            stop: float
            step_size: float

            def instantiate(self):
                return LinearSweepStepSize(self.start, self.stop,
                                           self.step_size)


class LinearSweepNumSteps(Sweep):
    start: float
    stop: float
    num_steps: int

    def __init__(self, start: float, stop: float, num_steps: int):
        self.start = start
        self.stop = stop
        self.num_steps = num_steps

    def __iter__(self):
        for i in range(self.num_steps):
            yield self.start + (self.stop - self.start)*i/(self.num_steps-1)

    if pydantic:
        class PydanticModel(Sweep.PydanticModel):
            type_: Literal["LinearSweepNumSteps"]
            start: float
            stop: float
            num_steps: int

            def instantiate(self):
                return LinearSweepNumSteps(self.start, self.stop,
                                           self.num_steps)


class ListSweep(Sweep):
    values: list[float]

    def __init__(self, values: list[float]):
        self.values = values

    def __iter__(self):
        for value in self.values:
            yield value

    if pydantic:
        class PydanticModel(Sweep.PydanticModel):
            type_: Literal["ListSweep"]
            values: list[float]

            def instantiate(self):
                return ListSweep(self.values)
