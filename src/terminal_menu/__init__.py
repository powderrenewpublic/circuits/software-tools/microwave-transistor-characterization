import functools
import typing

import simple_term_menu

class TerminalMenuDictionary(simple_term_menu.TerminalMenu):
    class _Viewport(simple_term_menu.TerminalMenu.Viewport):
        def __init__(self, unused_lines: int, *args, **kwargs):
            self.unused_lines = unused_lines
            super().__init__(*args, **kwargs)

        def _calculate_num_lines(self) -> int:
            return super()._calculate_num_lines() - self.unused_lines

    def __init__(self,
                 dict_menu_entries: dict[str, typing.Any],
                 unused_lines: int=10,
                 **kwargs):
        self.Viewport = functools.partial(self._Viewport, unused_lines)
        self.dict_menu_entries = dict_menu_entries
        self.dict_key_list = list(dict_menu_entries.keys())
        super().__init__(self.dict_key_list, **kwargs)

    def show(self, *args, **kwargs):
        result = super().show(*args, **kwargs)
        result_key = self.dict_key_list[result]
        return result_key, self.dict_menu_entries[result_key]

    def show_stack(self):
        menu_stack = [self]

        while len(menu_stack) > 0:
            key, value = menu_stack[-1].show()
            if isinstance(value, TerminalMenuDictionary):
                menu_stack.append(value)
            elif value is None:
                menu_stack.pop()
            else:
                result = value()
                if result is not None:
                    menu_stack.append(value)
