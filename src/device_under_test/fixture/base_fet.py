# Check for extra early
import pydantic

import dataclasses
import time

import bench_equipment.base.power_supply
import bench_equipment.base.dmm

@dataclasses.dataclass
class BaseFETWorkbench:
    v_gs_supply: bench_equipment.base.power_supply.BaseVoltageSupply
    v_ds_supply: bench_equipment.base.power_supply.BaseVoltageSupply
    v_gs_meter: bench_equipment.base.dmm.BaseVoltageSensor
    v_ds_meter: bench_equipment.base.dmm.BaseVoltageSensor
    i_g_meter: bench_equipment.base.dmm.BaseCurrentSensor
    i_d_meter: bench_equipment.base.dmm.BaseCurrentSensor

class BaseFETParameters(pydantic.BaseModel):
    class AbsoluteMaximums(pydantic.BaseModel):
        class Range(pydantic.BaseModel):
            minimum: float
            maximum: float
        v_gs: Range
        v_ds: Range
        i_g: Range
        i_d: Range

    safe_v_gs: float
    safe_v_gs_threshold: float # Will move on once within margin of v_gs
    #safe_v_gs_timeout: float # TODO: Set a default
    i_d_q_maximum: float
    absolute_maximums: AbsoluteMaximums

class SafeVGSException(Exception):
    pass
