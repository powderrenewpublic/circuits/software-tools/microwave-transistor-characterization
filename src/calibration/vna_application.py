# We must have pydantic for this to work, so let's get at it
import pydantic

import argparse
import os
import sys


import pydantic.v1.utils
import yaml


from ldb.hdf5 import high_level as HDF5


import bench_equipment.base.instrument
import bench_equipment.keysight.b296Xa
import bench_equipment.keysight.vna
import calibration.vna


class Config(pydantic.BaseModel):
    workbench_inventory: dict[str,
                              pydantic.Subclasses(
                                  bench_equipment.base.instrument.Instrument)]

    model_config = pydantic.ConfigDict(defer_build=True)

    _instantiated_inventory = None

    def instantiate_inventory(self):
        if self._instantiated_inventory is None:
            self._instantiated_inventory = {
                key: value.instantiate()
                for key, value in self.workbench_inventory.items()
            }

        return self._instantiated_inventory


def main_setup(description):
    parser = argparse.ArgumentParser(description=description)

    # Python scoping SUCKS!
    config = [{}]

    def update_config(yaml_filename):
        with open(yaml_filename, "r") as yaml_file:
            data = yaml.load(yaml_file, Loader=yaml.Loader)

        config[0] = pydantic.v1.utils.deep_update(config[0], data)

    parser.add_argument("-f", "--yaml-file", action="append")
    parser.add_argument("vna_name")
    parser.add_argument("hdf5_filename")

    args = parser.parse_args()

    for yaml_filename in args.yaml_file:
        update_config(yaml_filename)

    config = config[0]

    config = Config(**config)

    inventory = config.instantiate_inventory()
    vna = inventory[args.vna_name]

    return args, vna


def main_save():
    args, vna = main_setup(description="""
        This tool saves the calibration information from a VNA to an HDF5 file.
    """.strip())

    if os.path.exists(args.hdf5_filename):
        print(f"File {args.hdf5_filename} already exists.")
        response = input("Would you like to delete it? [yN] ")
        if response.lower() == 'y':
            os.remove(args.hdf5_filename)
        else:
            print("Can't proceed, delete or try a new filename")
            sys.exit(1)

    with HDF5.File.open(args.hdf5_filename) as hdf5_file:
        calibration.vna.save_calibration(hdf5_file, vna)


def main_load():
    args, vna = main_setup(description="""
        This tool loads the calibration information from an HDF5 file to a VNA.
    """.strip())

    with HDF5.File.open(args.hdf5_filename, read_only=True) as hdf5_file:
        calibration.vna.load_calibration(hdf5_file, vna)
