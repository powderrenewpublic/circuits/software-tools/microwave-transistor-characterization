from ldb.hdf5 import high_level as HDF5
from ldb.hdf5._hdf5 import lib as hdf5_lib, ffi as hdf5_ffi


def save_calibration(hdf5_location, vna):
    sweep_type = vna.get_sweep_type()

    if sweep_type == "LIN":
        stimulus_metadata = [
            ("type", "linear", "str"),
            ("start", vna.get_start_frequency(), HDF5.BuiltinTypes.IEEE_F64LE),
            ("stop", vna.get_stop_frequency(), HDF5.BuiltinTypes.IEEE_F64LE),
            ("points", vna.get_num_points(), HDF5.BuiltinTypes.STD_I64LE),
        ]
    else:
        raise Exception("Sweep types other than linear not supported yet.")

    stimulus = vna.get_calibration_stimulus()
    error_terms = vna.get_all_calibration_error_terms()
    assert b"Stimulus" not in error_terms

    # Figure out and check number of points
    assert len(stimulus) % 8 == 0 # Should be an array of doubles
    cal_num_points = len(stimulus) // 8
    for name, terms in error_terms.items():
        # Two floats per point
        assert len(terms) == 8*cal_num_points, \
                f"Wrong amount of data for {name}: {len(terms)} vs " \
                f"{4*cal_num_points}"

    print(cal_num_points)
    with HDF5.Space.create_simple([cal_num_points]) as space, \
            HDF5.Space.create_simple([2*cal_num_points]) as space2:
        stimulus_dataset = hdf5_location.create_dataset(
            "Stimulus", HDF5.BuiltinTypes.IEEE_F64BE, space)

        with HDF5.Space.create_scalar() as space:
            for metadata in stimulus_metadata:
                name, value, type_ = metadata
                if type_ == "str":
                    with HDF5.BuiltinTypes.FORTRAN_S1.copy() as file_type:
                        file_type.size = HDF5.H5T.VARIABLE
                        with stimulus_dataset.create_attribute(name, file_type,
                                                               space) as attr:
                            attr.scalar = value
                else:
                    with stimulus_dataset.create_attribute(name, type_,
                                                           space) as attr:
                        attr.scalar = value


        stimulus_data = hdf5_ffi.new(f"char[{len(stimulus)}]", stimulus)
        stimulus_dataset.ll_dataset.write(HDF5.BuiltinTypes.IEEE_F64BE.ll_type,
                                          stimulus_data)

        for error_term_name, error_term_data in error_terms.items():
            dataset = hdf5_location.create_dataset(
                error_term_name.decode("ascii"), HDF5.BuiltinTypes.IEEE_F32BE,
                space2)
            data = hdf5_ffi.new(f"char[{len(error_term_data)}]",
                                error_term_data)
            dataset.ll_dataset.write(HDF5.BuiltinTypes.IEEE_F32BE.ll_type,
                                     data)


def load_calibration(hdf5_location, vna):
    cal_set_name = "tmp_script_cal_set"
    stimulus_name = "Stimulus"

    with hdf5_location.open_dataset(stimulus_name) as stimulus_dataset:
        sweep_type = stimulus_dataset.open_attribute("type").scalar
        print(f"Sweep type: {sweep_type}")
        if sweep_type == "linear":
            start = stimulus_dataset.open_attribute("start").scalar
            stop = stimulus_dataset.open_attribute("stop").scalar
            points = stimulus_dataset.open_attribute("points").scalar
            print(start, stop, points)

            vna.set_sweep_type("LIN")
            vna.set_start_frequency(start)
            vna.set_stop_frequency(stop)
            vna.set_num_points(points)

            cal_set_names = vna.get_calibration_set_names()
            if cal_set_name in cal_set_names:
                vna.delete_calibration_set(cal_set_name)

            vna.create_calibration_set(cal_set_name)

        length = stimulus_dataset.space.simple_extent_dims[0]
        data = hdf5_ffi.new(f"char[{length*8}]")
        stimulus_dataset.ll_dataset.read(HDF5.BuiltinTypes.IEEE_F64BE.ll_type,
                                         data)

        cal_set_stimulus = vna.get_calibration_stimulus()

        assert bytes(hdf5_ffi.buffer(data)) == cal_set_stimulus


    for error_term_name in hdf5_location.list_group_contents():
        if error_term_name == stimulus_name:
            continue

        with hdf5_location.open_dataset(error_term_name) as error_term_dataset:
            length = error_term_dataset.space.simple_extent_dims[0]
            data = hdf5_ffi.new(f"char[{length*4}]")
            error_term_dataset.ll_dataset.read(
                HDF5.BuiltinTypes.IEEE_F32BE.ll_type, data)
            vna.write_error_term_data(error_term_name, hdf5_ffi.buffer(data))

    vna.save_calibration_set()
